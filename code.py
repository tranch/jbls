import web
import utils
from license_signer import LicenseSigner

urls = ('/rpc/obtainTicket.action', 'ObtainTicket',
        '/rpc/ping.action', 'Ping',
        '/rpc/releaseTicket.action', 'ReleaseTicket',
        '/_ah/health', 'HealthCheck',
        '/', 'HealthCheck',)

class ObtainTicket:
    def GET(self):
        form = web.input(userName='anonymous',
                         productCode='',
                         buildDate='',
                         clientVersion='',
                         machineId='',
                         hostName='',
                         buildNumber='',
                         productFamilyId='',
                         versionNumber='',
                         version='',
                         salt='',
                         secure='')

        data = {'message': ' ',
                'prolongationPeriod': 607875500,
                'responseCode': 'OK',
                'salt': form.salt,
                'ticketId': self._getTicketId(form.userName),
                'ticketProperties': self._getTicketProps(form.userName)}
        content_xml = utils.toXML('ObtainTicketResponse', data)

        license_signer = LicenseSigner()
        signature = license_signer.generate_signature(content_xml)
        comment_xml = utils.toXMLComment(signature)
        
        web.header('Content-Type', 'text/xml')
        return comment_xml + '\n' + content_xml

    def _getTicketId(self, userName):
        return reduce(lambda a, b: a + b, map(ord, list(userName)))
        
    def _getTicketProps(self, userName):
        return ' '.join(map(lambda x: '='.join(x), [('license', userName),
                                                    ('licenseType', '0')]))


class Ping:
    def GET(self):
        form = web.input(salt='', isAnswer='')
        data = {'message': '',
                'responseCode': 'OK',
                'salt': form.salt}

        web.header('Content-Type', 'text/xml')
        return utils.toXML('PingResponse', data)


class ReleaseTicket:
    def GET(self):
        form = web.input(salt='')
        data = {'message': '',
                'responseCode': 'OK',
                'salt': form.salt}

        web.header('Content-Type', 'text/xml')
        return utils.toXML('ReleaseTicketResponse', data)


class HealthCheck:
    """For GAE health checking"""

    def GET(self):
        return 'OK'


if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()
