import struct
import rsa

class LicenseSigner:
    def __init__(self):
        """
        Load RSA private key extracted from JetBrains License Server
        """
        with open('private_key.pem') as f:
            key_pem = f.read()
            self.private_key = rsa.PrivateKey.load_pkcs1(keyfile=key_pem,
                                                         format='PEM')
            f.close()

    @staticmethod
    def digit_char(d):
        return (48 + d) if d < 10 else (97 + d) - ord('\n')

    def generate_signature(self, message):
        """
        Signs the message with MD5 with RSA
        :param message: Text message to sign
        :return:        Hex string of message signature
        """

        signature = rsa.sign(message=message, priv_key=self.private_key, hash='MD5')
        format_bytes = '%ib' % len(signature)
        signature_bytes = struct.unpack(format_bytes, signature)
        hex_signature = []

        for signature_byte in signature_bytes:
            hex_signature.append(self.digit_char(signature_byte >> 4 & 0xF))
            hex_signature.append(self.digit_char(signature_byte & 0xF))

        return ''.join(map(chr, hex_signature))

