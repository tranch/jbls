import xml.etree.cElementTree as cet

def toXML(parent_tag, data):
    """
    Convert dict to xml string
    :param parent_tag:  root tag name
    :param data:        the data dict
    :return:            xml string
    """
    root = cet.Element(parent_tag)
    for k, v in data.iteritems():
        node = cet.SubElement(root, k)
        node.text = str(v)
    return cet.tostring(root)

def toXMLComment(content):
    comment = cet.Comment(' ' + content + ' ')
    return cet.tostring(comment)
